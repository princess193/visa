//
//  VisaController.swift
//  App
//
//  Created by Petar Mataic on 2018-05-13.
//

import Foundation
import Vapor

final class VisaController {
    
    private let countries: [CountryCode: Country]
    
    init() throws {
        let data = passport.convertToData()
        let decoder = JSONDecoder()
        let countries = try decoder.decode([Country].self, from: data)
        
        var dictionary = [CountryCode: Country]()
        for country in countries {
            dictionary[country.countryCode] = country
        }
        
        self.countries = dictionary
    }
    
    func index(_ req: Request) throws -> VisaResponse {
        let visaRequest = try req.query.decode(VisaRequest.self)
        
        let fromCountry = self.countries[visaRequest.to]!
        let visaType = fromCountry.visaTypeFor(country: visaRequest.from)
        let visaResponse = VisaResponse(from: visaRequest.from, to: visaRequest.to, visaRequired: visaType)
        return visaResponse
    }
    
}
