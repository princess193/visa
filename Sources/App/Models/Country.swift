//
//  Country.swift
//  App
//
//  Created by Petar Mataic on 2018-05-13.
//

import Vapor

struct Country: Codable {
    // Swift on Linux does not yet support automatic snake case translation
    enum CodingKeys: String, CodingKey {
        case countryCode = "country_code",
        visaFree = "visa_free",
        visaOnArrival = "visa_on_arrival",
        electronicTravelAuthorization = "electronic_travel_authorization",
        electronicVisa = "electronic_visa"
    }
    
    let countryCode: CountryCode
    let visaFree: [CountryCode]
    let visaOnArrival: [CountryCode]
    let electronicTravelAuthorization: [CountryCode]
    let electronicVisa: [String]
    
    func visaTypeFor(country: CountryCode) -> VisaType {
        if visaFree.contains(country) {
            return .visaFree
        } else if visaOnArrival.contains(country) {
            return .visaOnArrival
        } else if electronicTravelAuthorization.contains(country) {
            return .eletronicTravelAuthorization
        } else if electronicVisa.contains(country.rawValue) {
            return .electronicVisa
        } else {
            return .visa
        }
    }
}

enum CountryCode: String, Codable, Hashable {
    case af, al, dz, ad, ao, ag, ar, am, au, at, az, bs, bh, bd, bb, by, be, bz, bj, bt, bo, ba, bw, br, bn, bg, bf, bi, kh, cm, ca, cv, cf, td, cl, cn, co, km, cg, cd, cr, ci, hr, cu, cy, cz, dk, dj, dm, `do`, ec, eg, sv, gq, er, ee, et, fj, fi, fr, ga, gm, ge, de, gh, gr, gd, gt, gn, gw, gy, ht, hn, hk, hu, `is`, `in`, id, ir, iq, ie, il, it, jm, jp, jo, kz, ke, ki, rk, kw, kg, la, lv, lb, ls, lr, ly, li, lt, lu, mo, mk, mg, mw, my, mv, ml, mt, mh, mr, mu, mx, fm, md, mc, mn, me, ma, mz, mm, na, nr, np, nl, nz, ni, ne, ng, kp, no, om, pk, pw, ps, pa, pg, py, pe, ph, pl, pt, qa, ro, ru, rw, kn, lc, ws, sm, st, sa, sn, rs, sc, sl, sg, sk, si, sb, so, za, kr, ss, es, lk, vc, sd, sr, sz, se, ch, sy, tw, tj, tz, th, tl, tg, to, tt, tn, tr, tm, tv, ug, ua, ae, uk, us, uy, uz, vu, va, ve, vn, ye, zm, zw, z
}

enum VisaType: String, Codable {
    case visa, visaFree, visaOnArrival, eletronicTravelAuthorization, electronicVisa
}

struct VisaRequest: Content {
    let from: CountryCode
    let to: CountryCode
}

struct VisaResponse: Content {
    let from: CountryCode
    let to: CountryCode
    let visaRequired: VisaType
}

